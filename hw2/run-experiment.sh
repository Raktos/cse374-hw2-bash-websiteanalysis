#!/bin/bash

# Jason Ho
# CSE 374 HW2
# 22 January 2016

# I only want one input and one output file as parameters
if (( $# < 2 || $# > 2 ))
then
    echo "I need a single .txt list of urls and a single .txt for output" >&2
    exit 1
fi

# if the input file does not exist terminate
if [ ! -e $1 ]
then
    echo "input file not found" >&2
    exit 1
fi

#create the output file, overwrite if it already exists
> $2

# track rank
rank=1

# read the whole file in line by line
tail -n+1 $1 | while read line
do
    # perform the actual experiment
    echo "run-experiment: Performing measurement on $line..."
    size=$(./perform-measurement.sh $line)
    if ((size > 0))
    then # if the wget suceeded print the line
	echo "$rank $line $size" >> $2
	echo "run-experiment: ...Success"
    else # if it faield print nothing
	echo "run-experiment: ...Failed"
    fi
    ((rank++))
done

exit 0
