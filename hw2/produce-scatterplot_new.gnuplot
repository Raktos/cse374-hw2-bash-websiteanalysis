set terminal postscript eps 26

set xlabel "Rank"
set ylabel "Size (Bytes)"
set xrange [0:50]
set output "scatterplot_new.eps"
plot "results_new.txt" using 1:3 title "" with points

