Jason Ho
CSE374 HW2
22 January 2016

parse.sh
I removed the excess urls by only taking urls that had a <strong> tag preceding them in the line. Only the actual top 100 had this.
A run of the program confirmed 100 websites.
The first group in the regex gets this pattern specific to the file (along with the starting " of the url's href).
The second and third groups in the regex gets the url (generalizable).
The fourth group gets the trailing " and the rest of the string.

parse_new.sh
using Time's 2014 top 50 websites (they were not really ordered by rank, but i gave them an assumed rank in order of appearance)
http://time.com/3054279/50-best-websites-2014/
I removed the excess urls by only taking urls on a line beginning with ^<p><a href="
A simple run of the program showed I got exactly 50 urls, the number expected
The same rules apply as above, only the first group (file specific) was changed to this pattern. The remaining groups are the same in both parse scripts

Writeup
1)
line by line:
Names the X axis Rank
Names the Y axis Size (Bytes)
Sets the range of the x axis to be 0 to 100
Sets the output to scatterplot.eps
plots the file "results.txt" using a space seperated list, column 1 is rank, column 3 is size, they are generated as points

2)
There is no relationship between size and popularity. This is what I expected.
