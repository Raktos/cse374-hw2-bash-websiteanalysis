#!/bin/bash

# Jason Ho
# CSE374 HW 2
# 22 January 2016

# only take 1 URL as a parameter
if [[ $# -eq 0 || (($# > 1)) ]]
then
    echo "please provide one (1) url" >&2
    exit 1
fi

# get a tmp file
tmp=$(mktemp)

# trie 3 time and timeout at 10 seconds to download file
# send wget result to the tmp file
wget -q -t 3 -T 10 -O - $1 > $tmp

# if the file exists and is not of size 0 output its size
if [ $? -eq 0 ]
then
    echo $(wc -c < $tmp)
else # otherwise output 0
    echo "0"
fi

# remove the tmp file
rm -f $tmp

exit 0
